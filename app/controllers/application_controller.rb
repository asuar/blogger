class ApplicationController < ActionController::Base
    before_action :set_page

    def set_page
        @articles_months = Article.all.map { |d| d.created_at.strftime('%B') }.uniq
        @articles_years = Article.all.map { |d| d.created_at.strftime('%Y') }.uniq
    end

end
