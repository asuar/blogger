class ArticlesController < ApplicationController
    include ArticlesHelper

    before_action :require_login, only: [:new, :create, :edit, :update, :destroy]

    def index 
        @top_articles = Article.order(view_count: :desc).limit(3).includes(:tags)
        @number_of_articles = Article.count

        if(!params[:month].blank? && !params[:year].blank?)
            @header_title = "Articles for #{params[:month]}, #{params[:year]}"
            @articles = Article.by_year_and_month(params[:year],params[:month]).order(created_at: :desc).includes(:tags)
        else
            @header_title = "All Articles"
            @articles = Article.order(created_at: :desc).includes(:tags)
        end
    end

    def show
        @article = Article.find(params[:id])

        @article.increment_view_count
        @article.save
        
        @comment = Comment.new
        @comment.article_id = @article.id
    end

    def new
        @article = Article.new
    end

    def create
        @article = Article.new(article_params)
        @article.save

        flash.notice = "Article '#{@article.title}' Created!"

        redirect_to article_path(@article)
    end

    def edit
        @article = Article.find(params[:id])
    end

    def update
        @article = Article.find(params[:id])
        @article.update(article_params)

        flash.notice = "Article '#{@article.title}' Updated!"

        redirect_to article_path(@article)
    end

    def destroy
        @article = Article.find(params[:id])
        @article.destroy

        flash.notice = "Article '#{@article.title}' Deleted!"

        redirect_to articles_path
    end
end
