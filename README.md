# Blogger

Created with Ruby on Rails as part of The Odin Project [Curriculum](https://www.theodinproject.com/courses/web-development-101/lessons/ruby-on-rails). 

# Final Thoughts

Completing this project allowed me to learn how to build websites with Rails and review Ruby concepts. I also learned about using color palettes in the design of websites.
